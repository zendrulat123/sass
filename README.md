
There are two commands and their flags
- pull
  -  -p [path to pull files down]
  -  -o [path of other option scss files]
  -  -u [path of the style.css file]
  -  example ./sass pull -p static/ -a partials/ -c config/ -o static/
- sass
example command ./sass sass

Just download the binary [sass file here](https://gitlab.com/zendrulat123/sass/-/raw/main/sass?inline=false) 

Run this example 

./sass pull -p static/ -a partials/ -c config/ -o static/

Then run this to actually compile the files.  No need for flags because of the config.

./sass sass 


"pull" pulls everything down and writes to the config file so you dont have to do this again.
- -p is for pulling down the example scss file and making the path for compiling your scss files. 
- -a stands for any extra scss files you have in other directories.
- -c stands for where you want the config file. 
- -o stands for the output css file that has been compiled.

Currently only works on linux.



