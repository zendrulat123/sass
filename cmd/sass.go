/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/wellington/go-libsass"
)

// sassCmd represents the sass command
var sassCmd = &cobra.Command{
	Use:     "sass",
	Short:   "compiles the sass to css",
	Long:    `compiles the sass to css`,
	Example: `./sass sass`,
	Run: func(cmd *cobra.Command, args []string) {

		//path flag
		viper.SetConfigName("persis") // config file name without extension
		viper.SetConfigType("yaml")
		viper.AddConfigPath(".")
		viper.AddConfigPath("./config/") // config file path
		viper.AutomaticEnv()             // read value ENV variable
		err := viper.ReadInConfig()
		if err != nil {
			fmt.Println("fatal error config file: default \n", err)
			os.Exit(1)
		}
		path := viper.GetString("env.path")
		output := viper.GetString("env.output")
		partials := viper.GetString("env.partials")

		//create directory
		if err := os.MkdirAll(path, os.ModeSticky|os.ModePerm); err != nil {
			//	fmt.Println("Directory(ies) successfully created with sticky bits and full permissions")
		} else {
			fmt.Println(path + "/route already created")
		}
		// open input sass/scss file to be compiled
		fi, err := os.Open(path + "style.scss")
		if err != nil {
			panic(err)
		}
		defer fi.Close()

		// create output css file
		fo, err := os.Create(output + "style.css")
		if err != nil {
			panic(err)
		}
		defer fo.Close()

		// options for compilation
		p := libsass.IncludePaths([]string{partials})
		s := libsass.OutputStyle(libsass.COMPRESSED_STYLE)

		// create a new compiler with options
		comp, err := libsass.New(fo, fi, p, s)
		if err != nil {
			panic(err)
		}

		// start compile
		if err := comp.Run(); err != nil {
			panic(err)
		}

	},
}

func init() {
	rootCmd.AddCommand(sassCmd)
	sassCmd.Flags().StringP("path", "p", "", "Set your path")
	sassCmd.Flags().StringP("partials", "a", "", "Set your partials")
	sassCmd.Flags().StringP("output", "u", "", "Set your output")
}
