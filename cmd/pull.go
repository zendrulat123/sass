/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"
	"log"
	"os"
	"runtime"
	"sass/ut"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// pullCmd represents the pull command
var pullCmd = &cobra.Command{
	Use:     "pull",
	Short:   "pulls down scss and css files",
	Long:    `pulls down scss and css files`,
	Example: `./sass pull -p static/ -a partials/ -c config/ -o static/`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("pull called")

		path, _ := cmd.Flags().GetString("path")
		partials, _ := cmd.Flags().GetString("partials")
		config, _ := cmd.Flags().GetString("config")
		out, _ := cmd.Flags().GetString("output")

		//create path
		if err := os.MkdirAll(path, os.ModeSticky|os.ModePerm); err != nil {
			fmt.Println("Directory(ies) successfully created with sticky bits and full permissions")
		} else {
			fmt.Println(path + "/route already created")
		}

		fo, err := os.Create(path + "style.css")
		if err != nil {
			panic(err)
		}
		defer fo.Close()

		if out != "" {
			//create path
			if err := os.MkdirAll(out, os.ModeSticky|os.ModePerm); err != nil {
				fmt.Println("Directory(ies) successfully created with sticky bits and full permissions")
			} else {
				fmt.Println(out + "/route already created")
			}

			if runtime.GOOS == "windows" {
				err, out, errout := ut.ShelloutBash(" wget -O " + out + "style.scss https://raw.githubusercontent.com/golangast/sass/main/partial.scss")
				if err != nil {
					log.Printf("error: %v\n", err)
				}
				fmt.Println(out)
				fmt.Println("--- errs ---")
				fmt.Println(errout)
			} else {
				err, out, errout := ut.Shellout(" wget -O " + out + "style.scss https://raw.githubusercontent.com/golangast/sass/main/partial.scss")
				if err != nil {
					log.Printf("error: %v\n", err)
				}
				fmt.Println(out)
				fmt.Println("--- errs ---")
				fmt.Println(errout)
			}

		}

		if partials != "" {
			//create path
			if err := os.MkdirAll(partials, os.ModeSticky|os.ModePerm); err != nil {
				fmt.Println("Directory(ies) successfully created with sticky bits and full permissions")
			} else {
				fmt.Println(partials + "/route already created")
			}

			if runtime.GOOS == "windows" {
				err, out, errout := ut.ShelloutBash(" wget -O " + partials + "style.scss https://raw.githubusercontent.com/golangast/sass/main/partial.scss")
				if err != nil {
					log.Printf("error: %v\n", err)
				}
				fmt.Println(out)
				fmt.Println("--- errs ---")
				fmt.Println(errout)
			} else {
				err, out, errout := ut.Shellout(" wget -O " + partials + "style.scss https://raw.githubusercontent.com/golangast/sass/main/partial.scss")
				if err != nil {
					log.Printf("error: %v\n", err)
				}
				fmt.Println(out)
				fmt.Println("--- errs ---")
				fmt.Println(errout)
			}

		}

		if config != "" {
			ut.CreateConfig(config)
			ut.ConfigAddEnv(config+"/persis.yaml", "env:")
			ut.ConfigAddFile(config+"/persis.yaml", " path: "+"\""+path+"\"")
			ut.ConfigAddFile(config+"/persis.yaml", " partials: "+"\""+partials+"\"")
			ut.ConfigAddFile(config+"/persis.yaml", " config: "+"\""+config+"\"")
			ut.ConfigAddFile(config+"/persis.yaml", " output: "+"\""+out+"\"")

			viper.SetConfigName("persis") // config file name without extension
			viper.SetConfigType("yaml")
			viper.AddConfigPath(".")
			viper.AddConfigPath("./config/") // config file path
			viper.AutomaticEnv()             // read value ENV variable
			err := viper.ReadInConfig()
			if err != nil {
				fmt.Println("fatal error config file: default \n", err)
				os.Exit(1)
			}

		}

		if runtime.GOOS == "windows" {
			err, out, errout := ut.ShelloutBash(" wget -O " + path + "style.scss https://raw.githubusercontent.com/golangast/sass/main/style.scss  && go get -u github.com/wellington/go-libsass")
			if err != nil {
				log.Printf("error: %v\n", err)
			}
			fmt.Println(out)
			fmt.Println("--- errs ---")
			fmt.Println(errout)
		} else {
			err, out, errout := ut.Shellout(" wget -O " + path + "style.scss https://raw.githubusercontent.com/golangast/sass/main/style.scss  && go get -u github.com/wellington/go-libsass")
			if err != nil {
				log.Printf("error: %v\n", err)
			}
			fmt.Println(out)
			fmt.Println("--- errs ---")
			fmt.Println(errout)
		}
	},
}

func init() {
	rootCmd.AddCommand(pullCmd)
	pullCmd.Flags().StringP("path", "p", "", "Set your path")
	pullCmd.Flags().StringP("partials", "a", "", "Set your partials")
	pullCmd.Flags().StringP("config", "c", "", "Set your config")
	pullCmd.Flags().StringP("output", "o", "", "Set your output")

}
